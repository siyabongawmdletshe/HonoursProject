﻿using UnityEngine;
using System.Collections;
using GC_Framework;
using UnityEngine.Networking;
/**
 *This class represents the processes behind Artificial Intelligence applied on zombies
*/
namespace Zombies{
public class Zombie_AI: NetworkedObject {

		//Zombie Motions
		[SyncVar] private Vector3 syncPos;
		[SyncVar] private float syncYRot;
		
		private Vector3 lastPos;
		private Quaternion lastRot;
		private Transform myTransform1;
		private float lerpRate  =0.0000000001f;
		private float posThreshold = 0.000001f;
		private float rotThreshold = 0.000001f;

	private NavMeshAgent agent;
	private Transform myTransform;
	public Transform targetTransform;
	private LayerMask raycastLayer;
	private float radius = 100;

	public int health = 100;


	private float attackRate  = 3;
	private float nextAttack;
	public int Zombie_damage = 5;
	public int Player_damage = 5;
	private float minDistance = 2;
	private float currentDistance;
	
	private Zombie_Target targetScript;
	private Player_Troll2 PlayerScript;

	private Animation Zombie_animation;
	private bool setAnimation=false;
	



	// Use this for initialization
	public override void Start () 
	{
		base.Start ();
		agent = GetComponent<NavMeshAgent>();
		myTransform = transform;
		raycastLayer = 1<<LayerMask.NameToLayer("Player");
		myTransform = transform;
		targetScript = GetComponent<Zombie_Target>();
		PlayerScript = GetComponent<Player_Troll2>();
		Zombie_animation=GetComponent<Animation>(); 
		//Zombie_animation=GetComponent<Animator>(); 



		if(isServer)
		{
			StartCoroutine(DoCheck());
			StartCoroutine(Attack());
		}
	}



	public override void Update() {
		  base.Update ();
			TransmitMotion();
			LerpMotion();
	}

		//Zombie Motion
		void TransmitMotion()
		{
			if(!isServer)
			{
				return;
			}
			
			if(Vector3.Distance(myTransform.position, lastPos) > posThreshold || Quaternion.Angle(myTransform.rotation, lastRot)> rotThreshold)
			{
				lastPos = myTransform.position;
				lastRot = myTransform.rotation;
				
				syncPos = myTransform.position;
				syncYRot = myTransform.localEulerAngles.y;
			}
		}
		
		void LerpMotion()
		{
			if(isServer)
			{
				return;
			}
			
			myTransform.position = Vector3.Lerp(myTransform.position, syncPos, Time.deltaTime * lerpRate);
			
			Vector3 newRot = new Vector3(0, syncYRot, 0);
			myTransform.rotation = Quaternion.Lerp(myTransform.rotation, Quaternion.Euler(newRot), Time.deltaTime * lerpRate);
		}





//Find Player
	void SearchForTarget()
	{
		if(!isServer)
		{
			return;
		}

		if(targetTransform == null)
		{
			Collider[] hitColliders = Physics.OverlapSphere(myTransform.position, radius, raycastLayer);

			if(hitColliders.Length>0)
			{
				int randomint = Random.Range(0, hitColliders.Length);
				targetTransform = hitColliders[randomint].transform;
			}
		}

		if(targetTransform != null && targetTransform.GetComponent<CapsuleCollider>().enabled == false)
		{
			targetTransform = null;
		}
	}

	void MoveToTarget()
	{
		if(targetTransform != null && isServer)
		{
			SetNavDestination(targetTransform);
		}
	}

	void SetNavDestination(Transform dest)
	{
		agent.SetDestination(dest.position);

	}

	IEnumerator DoCheck()
	{
		for(;;)
		{
			SearchForTarget();
			MoveToTarget();
			yield return new WaitForSeconds(0.2f);
		}
	}


	//Attack Player
	void CheckIfTargetInRange()
	{
		if(targetScript.targetTransform != null)
		{   // Zombie_animation.Play("run");
			currentDistance = Vector3.Distance(targetScript.targetTransform.position, myTransform.position);
			
			if(currentDistance < minDistance && Time.time > nextAttack)
			{     setAnimation=true;
				//Zombie_animation.Play("Idle_01");
				nextAttack = Time.time + attackRate;
				
				if(setAnimation==true){
					Zombie_animation.Play("skill0");
						StartCoroutine(Run());

				}

					targetScript.targetTransform.GetComponent<Player_Troll2>().DeductHealth(Player_damage);
					targetScript.targetTransform.GetComponent<Player_Troll2>().anim.Play("Attack_02");

				
			}
			
			else{

				setAnimation=false;
				//Zombie_animation.Play("run");

				
			}
		}
	}
	
	
	IEnumerator Attack()
	{
		for(;;)
		{
			yield return new WaitForSeconds(0.2f);
			CheckIfTargetInRange();
		}
	}



		IEnumerator Run()
		{
			for(;;)
			{
				yield return new WaitForSeconds(0.2f);
				Zombie_animation.Play("run");
				CheckIfTargetInRange();
			}
		}




	//Destroy Zombie
	void OnTriggerEnter(Collider other) {
		
		if (other.gameObject.CompareTag ("Bomb")) {
				DeductHealth(Zombie_damage);
			
		}
		
		}


	void DeductHealth (int dmg)
	{
		health -= dmg;
		CheckHealth();
	}
	


	void CheckHealth()
	{
		if(health == 0)
		{

			targetScript.targetTransform.GetComponent<Player_Troll2>().CmdDestroyZombies(netId);
		
		}
	}


	}
}
