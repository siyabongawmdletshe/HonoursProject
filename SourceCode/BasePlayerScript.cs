﻿using UnityEngine;
using System.Collections;
using GC_Framework;
using UnityEngine.Networking;
using System.Collections.Generic;
/**
 *This class represents the base player object
*/

namespace Zombies

{
	[RequireComponent(typeof (Animator))]
	[RequireComponent(typeof (CapsuleCollider))]

public class BasePlayerScript: NetworkedPlayer
{

	public float animSpeed = 1.5f;				
	public float lookSmoother = 3.0f;			
	public bool useCurves = true;				
											

	public float useCurvesHeight = 0.5f;
	public float forwardSpeed = 7.0f;

	public float backwardSpeed = 2.0f;

	public float rotateSpeed = 2.0f;

	public float jumpPower = 3.0f; 

	
	

	

	private float orgColHight;
	private Vector3 orgVectColCenter;
	
								
	private AnimatorStateInfo currentBaseState;	

	private GameObject cameraObject;
	public Camera currentCamera;
		public Animator anim;
		private CapsuleCollider col;
		private Rigidbody rb;
		private Vector3 velocity;

	public GameObject bullet_prefab;	 
	

	static int idleState = Animator.StringToHash("Base Layer.Idle");
	static int locoState = Animator.StringToHash("Base Layer.Locomotion");
	static int jumpState = Animator.StringToHash("Base Layer.Jump");
	static int restState = Animator.StringToHash("Base Layer.Rest");
		bool ShootFoward=false;
		bool ShootBack=false;
		float bulletImpulse = 800f;
		public float PlayerSpeed=1.0f;


	public override void Start ()
	{

		base.Start();

		  ClientScene.RegisterPrefab(bullet_prefab);


		if (isLocalPlayer) {
		
			anim = GetComponent<Animator> ();

			col = GetComponent<CapsuleCollider> ();
			rb = GetComponent<Rigidbody> ();

			GameObject.Find("Camera").SetActive(false);
			currentCamera.enabled = true;

			orgColHight = col.height;
			orgVectColCenter = col.center;



		}


		}
	

	protected override void ListenForPlayerInput ()
	{
			base.ListenForPlayerInput();

		float h = Input.GetAxis("Horizontal");				
		float v = Input.GetAxis("Vertical");				
		anim.SetFloat("Speed", v);							
		anim.SetFloat("Direction", h); 						
		anim.speed = animSpeed;							
		currentBaseState = anim.GetCurrentAnimatorStateInfo(0);	
		rb.useGravity = true;
		
		

		velocity = new Vector3(0, 0, v);	

		velocity = transform.TransformDirection(velocity);

		if (v > 0.1) {
			velocity *= forwardSpeed;		
		} else if (v < -0.1) {
			velocity *= backwardSpeed;
		}
		
		if (Input.GetButtonDown("Jump")) {	


			if (currentBaseState.nameHash == locoState){

				if(!anim.IsInTransition(0))
				{
						rb.AddForce(Vector3.up * jumpPower, ForceMode.VelocityChange);
						anim.SetBool("Jump", true);	
				}
			}
		}
		

	
		transform.localPosition += velocity * Time.fixedDeltaTime;

	
		transform.Rotate(0, h * rotateSpeed, 0);	
	





			if (Input.GetKey(KeyCode.B))
			{
				ShootBack=true;
				ShootFoward=false;
				
				CmdServerSpawnPhysicsObject();
			}
			
			if (Input.GetKey(KeyCode.F))
			{
				ShootFoward=true;
				ShootBack=false;

				//GetComponent<Rigidbody>().constraints=	RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezePositionY;
				CmdServerSpawnPhysicsObject();
				//GetComponent<Rigidbody>().constraints=	RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
				anim.Play("Attack_01");

			}

  



		// locoState
		if (currentBaseState.nameHash == locoState){

			if(useCurves){
				resetCollider();
			}
		}

		// JUMP

		else if(currentBaseState.nameHash == jumpState)
		{
			//cameraObject.SendMessage("setCameraPositionJumpView");	

			if(!anim.IsInTransition(0))
			{
				

				if(useCurves){
				

					// GravityControl
					float jumpHeight = anim.GetFloat("JumpHeight");
					float gravityControl = anim.GetFloat("GravityControl"); 
					if(gravityControl > 0)
						rb.useGravity = false;	
										

					Ray ray = new Ray(transform.position + Vector3.up, -Vector3.up);
					RaycastHit hitInfo = new RaycastHit();
					// 高さが useCurvesHeight 以上ある時のみ、コライダーの高さと中心をJUMP00アニメーションについているカーブで調整する
					if (Physics.Raycast(ray, out hitInfo))
					{
						if (hitInfo.distance > useCurvesHeight)
						{
							col.height = orgColHight - jumpHeight;			// 調整されたコライダーの高さ
							float adjCenterY = orgVectColCenter.y + jumpHeight;
							col.center = new Vector3(0, adjCenterY, 0);	// 調整されたコライダーのセンター
						}
						else{
											
							resetCollider();
						}
					}
				}
				// Jump bool			
				anim.SetBool("Jump", false);
			}
		}
		// IDLE
		// 
		else if (currentBaseState.nameHash == idleState)
		{
		
			if(useCurves){
				resetCollider();
			}

			if (Input.GetButtonDown("Jump")) {
				anim.SetBool("Rest", true);
			}
		}
		// REST
	
		else if (currentBaseState.nameHash == restState)
		{
					
		
			if(!anim.IsInTransition(0))
			{
				anim.SetBool("Rest", false);
			}
		}
	}

		//Bullets
		[Command]
		void CmdServerSpawnPhysicsObject()
		{

			GameObject thebullet = (GameObject)Instantiate(bullet_prefab, transform.position +  new Vector3(transform.forward.x, transform.forward.y, transform.forward.z), transform.rotation);

			NetworkServer.Spawn(thebullet); 
			if (thebullet.GetComponent<BulletScript> () != null) {
				thebullet.GetComponent<BulletScript> ().StartMovingBullet (transform.forward * bulletImpulse * Time.deltaTime);
			}
		
			
		}

	void OnGUI()
	{
		GUI.Box(new Rect(Screen.width -260, 10 ,250 ,150), "Interaction");
		
		GUI.Label(new Rect(Screen.width -245,30,250,30),"Up/Down Arrow : Go Forwald/Go Back");
		GUI.Label(new Rect(Screen.width -245,50,250,30),"Left/Right Arrow : Turn Left/Turn Right");
		GUI.Label(new Rect(Screen.width -245,70,250,30),"Hit Space key while Running : Jump");
		GUI.Label(new Rect(Screen.width -245,90,250,30),"Hit Spase key while Stopping : Rest");
		GUI.Label(new Rect(Screen.width -245,110,250,30),"Hit F: Attack");


			GUI.Box(new Rect(Screen.width -600, 10 ,250 ,150), "Instructions");
			GUI.Label(new Rect(Screen.width -590,30,250,30),"The target point is the locked house.");
			GUI.Label(new Rect(Screen.width -590,50,250,30),"Find 10 keys then go unlock the house.");
			GUI.Label(new Rect(Screen.width -590,70,250,40),"Whoever finds 10 keys and gets to the house first, wins.");
			GUI.Label(new Rect(Screen.width -590,100,250,30),"Red boxes represents keys.");
			GUI.Label(new Rect(Screen.width -590,120,250,30),"Remember, Zombies kill!");

		
	}



	void resetCollider()
	{
	
		col.height = orgColHight;
		col.center = orgVectColCenter;
	}
}

}
