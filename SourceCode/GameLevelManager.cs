﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using GC_Framework;
using UnityEngine.UI;
/**
 *This class registers / spawns the zombie on the network
*/

namespace Zombies
{
	
	
	public class GameLevelManager : LevelManager    {
		[SerializeField] GameObject zombiePrefab;
		[SerializeField] GameObject ThisFinalHouse;
		
		[SerializeField]GameObject DestinationPoints;
		private int counter;
		public int numberOfZombies=10;
		public int NumberOfDestinations=5;
		private PlaySounds playsound;

		
		public float min_x, max_x, min_z, max_z ;



		private GOM GameOverScript;
		
		public List<NetworkedPlayer> getPlayers(){
			return playerObjects;
		}
		
		
		Vector3 GenerateRandomPositions()
		{
			float x,y,z;
			x = Random.Range(min_x,max_x);
			y = 0;
			z = Random.Range(min_z,max_z);
			return new Vector3(x,y,z);
		}


		Vector3 GenerateRandomPositions1()
		{
			float x,y,z;
			x = Random.Range(min_x,max_x);
			y = 1;
			z = Random.Range(min_z,max_z);
			return new Vector3(x,y,z);
		}
		
         //spawn zombies
		protected override void OnLevelBegin()
		{
			playsound=GetComponent<PlaySounds>();
			base.OnLevelBegin();
			StartCoroutine (PlayBackgroundMusic());

			if (isServer)
			{
				for (int i = 0; i < numberOfZombies; i++) {
					GameObject go = GameObject.Instantiate (zombiePrefab, GenerateRandomPositions(), Quaternion.identity) as GameObject;	
					NetworkServer.Spawn (go);

				}



				Transform myTr=ThisFinalHouse.transform;
				Vector3 v=new Vector3(myTr.position.x, myTr.position.y, myTr.position.z);
				GameObject FinalH = GameObject.Instantiate (ThisFinalHouse, v, Quaternion.identity) as GameObject;	
				NetworkServer.Spawn (FinalH);



				//Spawn Destination Points

				for (int i=0; i<NumberOfDestinations; i++) {
					
					GameObject DesP = GameObject.Instantiate (DestinationPoints, GenerateRandomPositions1(), Quaternion.identity) as GameObject;
					 NetworkServer.Spawn (DesP);
				}
			}
		}

	 

		public void Cmdloadscene(NetworkInstanceId myid){
			
			LevelManager.LoadNewScene ("Gameover");
		}





		IEnumerator PlayBackgroundMusic()
		{

			yield return new WaitForSeconds(0.2f);

			playsound.myPlaySounds (0,20.0f,true,true);

		}

	}
}

