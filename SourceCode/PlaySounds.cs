﻿using UnityEngine;
using System.Collections;
using GC_Framework;
using UnityEngine.Networking;

namespace Zombies{
	/**
 *This class is responsible for background sound
*/
public class PlaySounds : AudioManager{
	float _time = 0;
	bool _loop = false;
	int _id;
	float _volume;

	public void myPlaySounds(int id, float volume, bool snc, bool loop) {
		PlaySound (id, volume,snc);
		_loop = loop;
		_id = id;
		_volume = volume;
	}

	 void Update(){
		if (!_loop)
			return;

		_time += Time.deltaTime;

		if(_time > audio_clips[_id].length){
			_time=0;
			myPlaySounds(_id,_volume,true,_loop);
		}
	} 
}


}
