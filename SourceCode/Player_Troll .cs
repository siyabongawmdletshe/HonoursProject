﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using GC_Framework;

namespace Zombies{
	
	/**
 *This class represents the sub class player inheriting from BasePlayerScript 
*/
public class Player_Troll : BasePlayerScript {


	[SyncVar (hook = "OnHealthChanged")] private int health = 100;
	[SyncVar (hook = "OnKeyChanged")] private int keys = 0;

	
	private Text healthText;
	private Text KeysText;


				public Animator anims;
		private PlaySounds playsound;
	private Text WinnerText;
	//private Text GameOverWinnerText;
	private GameObject winnerT;
	
	
	private bool shouldDie = false;
	public bool isDead = false;
		static int countme=0;
	public delegate void DieDelegate();
	public event DieDelegate EventDie;
	
	public delegate void RespawnDelegate();
	public event RespawnDelegate EventRespawn;
	

	private PlayerLevelManager LevelScript;
	private GameOverManager Gom;
		private GameObject canvasObject;

     public GameObject WinnerTex;
		//[SerializeField]GameObject WinnerTex;

		NetworkInstanceId id;
		private bool finalhouse=false;

	// Use this for initialization
	public override void Start () {
		base.Start();
		ClientScene.RegisterPrefab(bullet_prefab);

		  if(isLocalPlayer){
			
			    healthText = GameObject.Find("Health Text").GetComponent<Text>();
				KeysText =  GameObject.Find("Key Text").GetComponent<Text>();
				WinnerText =  GameObject.Find("WinText").GetComponent<Text>();
				LevelScript=GameObject.Find("PlayerLevelManager").GetComponent<PlayerLevelManager>();
				//canvasObject =  GameObject.Find("Canvas 2");
				//Gom= GetComponent<GameOverManager>();
			    SetHealthText();
				SetKeyText();

			  anims = GetComponent<Animator> ();
			  playsound=GetComponent<PlaySounds>();
		}



	
	}
	
	// Update is called once per frame
	protected override void ListenForPlayerInput () {
			base.ListenForPlayerInput ();
		      CheckCondition();
			  

	}

	void CheckCondition()
	{

		if(health <= 0 && !shouldDie && !isDead)
		{
			shouldDie = true;
		}
		
		if (health <= 0 && shouldDie) {
			

			StartCoroutine (PlayDeathSound ());
		}


		
		
	}

	
	void StopEvent(){
		
		if(EventDie != null)
		{  
			
			EventDie();
			
		}
		
		shouldDie = false;
	}

	void SetHealthText()
	{
		if(isLocalPlayer)
		{
			healthText.text = "Life " + health.ToString();
			
			
		}
	}


		void SetKeyText()
		{
			if(isLocalPlayer)
			{
				KeysText.text = "Keys " + keys.ToString();
			
				
			}
		}
	


	public void DeductHealth (int dmg)
	{
		health -= dmg;
		StartCoroutine (PlayAttackSound ());
	}
	

		public void AddKey (int dmg)
		{
			keys += dmg;
		
			checkStatus ();
		}




		void checkStatus(){
			if (finalhouse==true && keys>=10) {
				
				CmdloadGameOverscene(netId);
				
			}
		}

	void OnHealthChanged(int hlth)
	{
		health = hlth;
		SetHealthText();
	}

		void OnKeyChanged(int key)
		{
			keys = key;
			SetKeyText();
		}
	




	IEnumerator PlayAttackSound()
	{
		
		yield return new WaitForSeconds(0.2f);
		
		playsound.myPlaySounds (0,10.0f,false,false);

	}
	
	IEnumerator PlayDeathSound()
	{
		
		
		playsound.myPlaySounds (1,10.0f,false,false);
			yield return new WaitForSeconds(3.0f);
		StopEvent();
		
	}

	//Disable Player

	public override void PreStartClient ()
	{

		EventDie += DisablePlayer;
	}

	
	public override void OnNetworkDestroy ()
	{
		EventDie -= DisablePlayer;
	}
	
	void DisablePlayer ()
	{
		GetComponent<CapsuleCollider> ().enabled = false;
		isDead = true;
		//gameObject.SetActive (false);
			CmdDestroyPlayer ();
	 }


		[Command]
		void CmdDestroyPlayer(){
		
			GameObject theObject = NetworkServer.FindLocalObject(netId);
			NetworkServer.Destroy(theObject);
		}


	[Command]
	public void CmdDestroyZombies(NetworkInstanceId applyDamageTo)
	{
		GameObject theObject = NetworkServer.FindLocalObject(applyDamageTo);
		NetworkServer.Destroy(theObject);
		
	}




		[Command]
		public void CmdDestroyPickUps(GameObject applyDamageTo)
		{

			NetworkServer.Destroy(applyDamageTo);
			
		}


		//Load NEW SCENE


		void CmdloadGameOverscene(NetworkInstanceId myid){
			//WinnerText.text = "Winner : " + myid;
			//DontDestroyOnLoad (canvasObject);
			LevelManager.LoadNewScene ("Gameover");

		}


		void OnTriggerEnter(Collider other) {
			if(isServer){

				if (other.gameObject.CompareTag ("House")){


					AddKey(1);
					CmdDestroyPickUps(other.gameObject);
				}


				if (other.gameObject.CompareTag ("FinalHouse")){
					
					
					finalhouse=true;
					checkStatus ();
					CmdDestroyPickUps(other.gameObject);

				}




			}
				

		}

	


	}
}
